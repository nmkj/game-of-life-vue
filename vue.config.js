const isProduction = process.env.NODE_ENV === 'production'

module.exports = {
  publicPath: isProduction ? '/lifegame/' : '/',
  configureWebpack: {
    devtool: isProduction ? false : 'source-map'
  }
}
