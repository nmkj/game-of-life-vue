export class World {
  constructor({
    width = 20,
    height = 20,
    upper = 4,
    lower = 1,
    born = {
      upper: 4,
      lower: 3
    },
    hexagon = false
  } = {}) {
    this.width = width
    this.height = height
    this.threshold = { upper, lower, born }
    this.cells = this.newCells(height, width)
    this.hexagon = hexagon
    this.resetCellsCount()
  }

  incrementAliveCells() {
    ++this.aliveCells
    --this.deadCells
  }

  incrementDeadCells() {
    --this.aliveCells
    ++this.deadCells
  }

  resetCellsCount() {
    this.aliveCells = 0
    this.deadCells = this.width * this.height
  }

  clear() {
    this.cells = this.newCells(this.height, this.width)
  }

  newCells(row, col) {
    const cells = new Array(row)
    for (let r = 0; r < row; ++r) {
      cells[r] = new Array(col)
      for (let c = 0; c < col; ++c) {
        cells[r][c] = { row: r, column: c, status: 'dead' }
      }
    }
    this.resetCellsCount()

    return cells
  }

  random(possibility = 0.5) {
    this.cells.forEach(columns => {
      columns.forEach(cell => {
        if (Math.random() < possibility) {
          cell.status = 'alive'
          this.incrementAliveCells()
        } else {
          cell.status = 'dead'
        }
      })
    })
  }

  tickCells() {
    const next = this.cells.map((row, r) =>
      row.map((__, c) => this.nextCell(r, c))
    )

    return next
  }

  tick() {
    this.cells = this.tickCells()
  }

  moore(row, col) {
    let aliveCells = 0

    for (let r = -1; r <= 1; ++r) {
      for (let c = -1; c <= 1; ++c) {
        let nowRow = (row + r) % this.height
        let nowCol = (col + c) % this.width
        nowRow = nowRow < 0 ? this.height - 1 : nowRow
        nowCol = nowCol < 0 ? this.width - 1 : nowCol
        if (r === 0 && c === 0) {
          continue
        }

        const neighbor = this.cells[nowRow][nowCol]
        if (neighbor.status === 'alive') {
          ++aliveCells
        }
      }
    }

    return aliveCells
  }

  hex(row, col) {
    let aliveCells = 0

    for (let r = -1; r <= 1; ++r) {
      for (let c = -1; c <= 1; ++c) {
        const nowRow = row + r
        const nowCol = col + c

        if (
          (r !== 0 && (row % 2 === 0 ? c === 1 : c === -1)) ||
          (r === 0 && c === 0) ||
          nowRow < 0 ||
          nowRow > this.height - 1 ||
          nowCol < 0 ||
          nowCol > this.width - 1
        ) {
          continue
        }

        const neighbor = this.cells[nowRow][nowCol]
        if (neighbor.status === 'alive') {
          ++aliveCells
        }
      }
    }

    return aliveCells
  }

  nextCell(row, col) {
    const current = this.cells[row][col]
    let aliveCells = 0
    if (this.hexagon) {
      aliveCells = this.hex(row, col)
    } else {
      aliveCells = this.moore(row, col)
    }

    let next = current.status
    if (
      aliveCells <= this.threshold.lower ||
      aliveCells >= this.threshold.upper
    ) {
      next = 'dead'
      if (current.status !== next) {
        this.incrementDeadCells()
      }
    } else if (
      this.threshold.born.lower <= aliveCells &&
      aliveCells <= this.threshold.born.upper
    ) {
      next = 'alive'
      if (current.status !== next) {
        this.incrementAliveCells()
      }
    }

    return {
      row: row,
      column: col,
      status: next
    }
  }
}

export default {
  World
}
